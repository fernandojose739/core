# README #

* WebApi em dotnet core 
* Desenvolvimento em TDD
* Arquitetura DDD

### Versão ###

* Version 1.0

### Como configurar ###

* git clone https://fernandojose739@bitbucket.org/fernandojose739/core.git
* abrir o projeto no visual studio code
* Abrir o Terminal do VS Code e deixar na raiz do projeto
* dotnet restore
* dotnet build

### Tecnologias utilizadas ###

* dotnet core
* Testes em xunit
* Bogus para a criação de conteúdo
* Mock para o repository
* Injeção de Dependência

### Ainda esta por vir ###

* front em angular 5
* gulp para automatização de tarefas

### Extensões úteis para vs code ###

* C# for visual studio code
* C# Extensions
* C# FixFormat
* C# Snippers
* .Net Core Test Explorer
* Asp.Net Core Snippers
* Darcula Theme
* Visual Studio Keymap

